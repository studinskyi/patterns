package Creational.Structural.Linker;

public class District extends Yard {
    long areaDistrict;

    @Override
    public long countCitizens() {
        return super.countCitizens();
    }

    @Override
    public long budgetYards() {
        return super.budgetYards();
    }

    @Override
    public long areaDistricts() {
        return areaDistrict;
    }

    @Override
    public long taxCities() {
        return 0;
    }
}
