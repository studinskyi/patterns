package Creational.Structural.Linker;

public class City extends District {
    long taxCity;

    @Override
    public long countCitizens() {
        return super.countCitizens();
    }

    @Override
    public long budgetYards() {
        return super.budgetYards();
    }

    @Override
    public long areaDistricts() {
        return super.areaDistricts();
    }

    @Override
    public long taxCities() {
        return taxCity;
    }
}
