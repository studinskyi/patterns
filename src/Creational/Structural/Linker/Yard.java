package Creational.Structural.Linker;

public class Yard extends House{
    public long budgetYard;

    @Override
    public long countCitizens() {
        return super.countCitizens();
    }

    @Override
    public long budgetYards() {
        return budgetYard;
    }

    @Override
    public long areaDistricts() {
        return 0;
    }

    @Override
    public long taxCities() {
        return 0;
    }
}
