package Creational.Structural.Linker;

public class House implements IGovernment{
    public int countCitizen;

    @Override
    public long countCitizens() {
        return (long) countCitizen;
    }

    @Override
    public long budgetYards() {
        return 0;
    }

    @Override
    public long areaDistricts() {
        return 0;
    }

    @Override
    public long taxCities() {
        return 0;
    }


}
