package Creational.Structural.Linker;

public interface IGovernment {
    long countCitizens();
    long budgetYards();
    long areaDistricts();
    long taxCities();


}

