package Creational.Structural.Linker;

import java.util.ArrayList;
import java.util.List;

public class CompositeHouse implements IGovernment{

    private List<House> listHouses = new ArrayList<House>();

    public void add(House house) {
        listHouses.add(house);
    }

    public void remove(House house) {
        listHouses.remove(house);
    }

    @Override
    public long countCitizens() {
        return 0;
    }

    @Override
    public long budgetYards() {
        return 0;
    }

    @Override
    public long areaDistricts() {
        return 0;
    }

    @Override
    public long taxCities() {
        return 0;
    }
}
