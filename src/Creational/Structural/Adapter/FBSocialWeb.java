package Creational.Structural.Adapter;

import java.util.ArrayList;
import java.util.List;

public class FBSocialWeb {

    public String getFriend(long id) {
        return "friend" + id + " in Facebook";
    }

    public List<String> getWall(long id, int likes, boolean showHidden) {
        if (id > 1000000 && !showHidden) {
            return new ArrayList<>();
        } else {
            List<String> list = new ArrayList<>();
            list.add("FASEFASE");
            list.add("TROLOLO");
            return list;
        }
    }

    public void pay(float amount, float voluteCoef, long idReceiver) {
        System.out.println("You pay " + amount * voluteCoef + " to " + idReceiver + " in facebook!");
    }

}
