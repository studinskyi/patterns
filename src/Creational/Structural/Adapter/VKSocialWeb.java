package Creational.Structural.Adapter;

import java.util.ArrayList;
import java.util.List;

public class VKSocialWeb{

    public String getFriend(int id, boolean showHidden) {
        if (id < 1000 && !showHidden) {
            return "";
        } else {
            return "friend" + id;
        }
    }

    public List<String> getWall(int id, int likes, boolean showHidden) {
        if (id > likes) {
            return new ArrayList<>();
        } else {
            List<String> list = new ArrayList<>();
            list.add("ASASA");
            list.add("OLOLO");
            return list;
        }
    }

    public boolean pay(float amount, int idReceiver) {
        System.out.println("You pay " + amount + " to " + idReceiver + " in vk!");
        return true;
    }
}
