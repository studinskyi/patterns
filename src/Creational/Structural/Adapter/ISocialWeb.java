package Creational.Structural.Adapter;

import java.util.List;

public interface ISocialWeb {
    String getFriend(long id);

    List<String> getWall(long id, boolean showHidden);

    void pay(float amount, float voluteCoef, long idReceiver);

}
