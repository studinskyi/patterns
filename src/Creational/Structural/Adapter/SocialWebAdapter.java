package Creational.Structural.Adapter;

import java.util.List;

public class SocialWebAdapter implements ISocialWeb {
    private Object iSocialWeb;

    public SocialWebAdapter(Object iSocialWeb) {
        this.iSocialWeb = iSocialWeb;
    }

    @Override
    public String getFriend(long id) {
        if (iSocialWeb instanceof VKSocialWeb) {
            ((VKSocialWeb) iSocialWeb).getFriend((int) id, true);
        }
        if (iSocialWeb instanceof FBSocialWeb) {
            ((FBSocialWeb) iSocialWeb).getFriend(id);
        }
        return null;
    }

    @Override
    public List<String> getWall(long id, boolean showHidden) {
        if (iSocialWeb instanceof VKSocialWeb) {
            ((VKSocialWeb) iSocialWeb).getWall((int) id, (int) (Math.random() * 100), showHidden);
        }
        if (iSocialWeb instanceof FBSocialWeb) {
            ((FBSocialWeb) iSocialWeb).getWall((int)id, (int) (Math.random() * 100), showHidden);
        }

        return null;
    }

    @Override
    public void pay(float amount, float voluteCoef, long idReceiver) {
        if (iSocialWeb instanceof VKSocialWeb) {
            ((VKSocialWeb) iSocialWeb).pay((float) amount*voluteCoef, (int) idReceiver);
        }
        if (iSocialWeb instanceof FBSocialWeb) {
            ((FBSocialWeb) iSocialWeb).pay((float)amount, voluteCoef, idReceiver);
        }

    }
}
