package Creational.Structural.Decorator;

public class Loan implements Component{

    Component component;

    @Override
    public void addCondition() {
        if (component != null)
            component.addCondition();
        System.out.println("loans conditions");
    }
}
