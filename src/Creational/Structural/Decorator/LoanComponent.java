package Creational.Structural.Decorator;

public abstract class LoanComponent implements Component {
    // это и есть наш класс, реализующий паттерн декоратор
    Component component;

    public LoanComponent(Component component) {
        this.component = component;
    }

    @Override
    public void addCondition() {
        if (component != null)
            component.addCondition();
        System.out.println("loans conditions");
    }
}
