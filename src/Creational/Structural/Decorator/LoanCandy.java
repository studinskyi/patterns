package Creational.Structural.Decorator;

/**
 * Created by studi on 04.07.2017.
 */
public class LoanCandy extends LoanComponent {
    public LoanCandy(Component component) {
        super(component);
    }

    @Override
    public void addCondition() {
        super.addCondition();
        System.out.println("LoanCandy's conditions");
    }
}
