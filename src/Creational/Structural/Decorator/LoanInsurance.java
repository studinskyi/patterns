package Creational.Structural.Decorator;

/**
 * Created by studi on 04.07.2017.
 */
public class LoanInsurance extends LoanComponent {

    public LoanInsurance(Component component) {
        super(component);
    }

    @Override
    public void addCondition() {
        super.addCondition();
        System.out.println("LoanInsurance's conditions");
    }
}
