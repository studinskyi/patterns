package Creational.Structural.Decorator;

/**
 * Created by sa on 03.07.17.
 */
public class Main {
    public static void main(String[] args) {
        Loan loan = new Loan();
        LoanInsurance loanInsurance = new LoanInsurance(loan);
        LoanCandy loanCandy = new LoanCandy(loanInsurance);
        LoanRefinansingPossibility loanRefPos = new LoanRefinansingPossibility(loanCandy);
        loanRefPos.addCondition();
    }
}
