package Creational.Structural.Decorator;

public interface Component {
    public void addCondition();

}
