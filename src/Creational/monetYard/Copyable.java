package Creational.monetYard;

public interface Copyable {
    Copyable copy();
}
