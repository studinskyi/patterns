package Creational.bihavioral.observer;

public interface Observer {

    void message(Object object);
}
