package Creational.bihavioral.observer;

public class Main {
    public static void main(String[] args) {
        BankCard bankCard1 = new BankCard();
        BankCard bankCard2 = new BankCard();

        IRS irs = new IRS();
        BanckScore banckScore = new BanckScore();
        banckScore.addObserver(bankCard1);
        banckScore.addObserver(bankCard2);
        banckScore.notifyAllObserver();
        banckScore.removeObserver(bankCard2);
        banckScore.notifyAllObserver();
    }
}
