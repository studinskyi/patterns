package Creational.bihavioral.observer;

/**
 * Created by studi on 05.07.2017.
 */
public interface Observable {

    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyAllObserver();
}
