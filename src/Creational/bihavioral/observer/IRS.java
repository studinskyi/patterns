package Creational.bihavioral.observer;

/**
 * Created by studi on 05.07.2017.
 */
public class IRS implements Observer {
    @Override
    public void message(Object object) {
        System.out.println(object.toString());
    }
}
