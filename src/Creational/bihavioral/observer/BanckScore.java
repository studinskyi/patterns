package Creational.bihavioral.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by studi on 05.07.2017.
 */
public class BanckScore implements Observable {

    List<Observer> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);

    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);

    }

    @Override
    public void notifyAllObserver() {
        for (Observer observer : observers) {
            observer.message("balance changed");
        }

    }
}
