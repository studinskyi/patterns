package Creational.bihavioral.chain;

public class BabkyRumours extends Rumour {
    boolean isAlive;

    public BabkyRumours(boolean isAlive) {
        this.isAlive = isAlive;
    }

    @Override
    public void obs(String strRumour) {
        if (!isAlive) {
            System.out.println("Sorry, babka is ded.");
            return;
        }
        System.out.println("Newspaper wrote " + strRumour);
        if (next != null) {
            System.out.println("Babka soid " + strRumour);
            next.obs(strRumour);
        }
    }
}
