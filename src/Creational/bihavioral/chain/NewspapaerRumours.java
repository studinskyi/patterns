package Creational.bihavioral.chain;

public class NewspapaerRumours extends Rumour {
    Integer balance;

    public NewspapaerRumours(Integer balance) {
        this.balance = balance;
    }

    @Override
    public void obs(String strRumour) {
        if (balance <= 0) {
            System.out.println("Newspapaer is bancroupt");
            return;
        }
        System.out.println("Newspaper wrote " + strRumour);
        if (next != null) {
            next.obs(strRumour);
        }
    }
}
