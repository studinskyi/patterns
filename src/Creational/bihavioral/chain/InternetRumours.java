package Creational.bihavioral.chain;

public class InternetRumours extends Rumour{
    boolean isConnected;

    public InternetRumours(boolean isConnected) {
        this.isConnected = isConnected;
    }

    @Override
    public void obs(String strRumour) {
        if (!isConnected) {
            System.out.println("Internet is not connected");
            return;
        }
        System.out.println("Newspaper wrote in internet " + strRumour);
        if (next != null) {
            next.obs(strRumour);
        }
    }

}
