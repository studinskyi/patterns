package Creational.bihavioral.chain;

public abstract class Rumour implements Rumours {
    protected Rumour next;

    public void setNext(Rumour next) {
        this.next = next;
    }
}
