package Creational.bihavioral.chain;

public class Main {
    public static void main(String[] args) {
        BabkyRumours babkyRumours = new BabkyRumours(true);
        NewspapaerRumours newspapaerRumours = new NewspapaerRumours(1000);
        InternetRumours internetRumours = new InternetRumours(true);

        babkyRumours.setNext(newspapaerRumours);
        newspapaerRumours.setNext(internetRumours);
        babkyRumours.obs("Gref is coming");
    }
}
