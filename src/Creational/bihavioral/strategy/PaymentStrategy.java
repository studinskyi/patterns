package Creational.bihavioral.strategy;

/**
 * Created by studi on 05.07.2017.
 */
public interface PaymentStrategy {
    void pay(int amount);
}
