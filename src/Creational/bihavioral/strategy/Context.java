package Creational.bihavioral.strategy;

/**
 * Created by studi on 05.07.2017.
 */
public class Context {
    int cardBalance;
    int income;
    PaymentStrategy paymentStrategy;

    public Context(int cardBalance, int income) {
        this.cardBalance = cardBalance;
        this.income = income;
        calculateStrategy();
    }

    public void pay(int amount) {
        paymentStrategy.pay(amount);
    }

    private void calculateStrategy() {
        if (cardBalance < 0) {
            paymentStrategy = new EconomyStrategy();
        }
        if (cardBalance >= 0 && income < 1000) {
            paymentStrategy = new SimpleStrategy();
        }
        if (cardBalance >= 0 && income > 1000) {
            paymentStrategy = new FastStrategy();
        }

    }
}
