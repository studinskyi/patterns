package Creational.bihavioral.State;

public class VerifiedState implements State {
    @Override
    public void doAction(Context context) {
        System.out.println("Документ находится в состоянии Preparation");
        context.setState(this);
    }

    public String toString() {
        return "Preparation-состояние документа";
    }
}
