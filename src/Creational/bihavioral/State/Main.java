package Creational.bihavioral.State;

public class Main {
    public static void main(String[] args) {
        Context context = new Context();

        LoadState startState = new LoadState();
        startState.doAction(context);
        System.out.println(context.getState().toString());

        SignedState approvedState = new SignedState("1 клиент");
        approvedState.doAction(context);
        System.out.println(context.getState().toString());

        SentState sentState = new SentState();
        sentState.doAction(context);
        System.out.println(context.getState().toString());

        VerifiedState preparationState = new VerifiedState();
        preparationState.doAction(context);
        System.out.println(context.getState().toString());

        ReceivedClientState receivedClientState = new ReceivedClientState("2 клиент");
        receivedClientState.doAction(context);
        System.out.println(context.getState().toString());

        approvedState = new SignedState("2 клиент");
        approvedState.doAction(context);
        System.out.println(context.getState().toString());

        ConfirmationSentState confirmationSentState = new ConfirmationSentState();
        confirmationSentState.doAction(context);
        System.out.println(context.getState().toString());

    }
}
