package Creational.bihavioral.State;

public class SentState implements State {
    @Override
    public void doAction(Context context) {
        System.out.println("Документ находится в состоянии Sent");
        context.setState(this);
    }

    public String toString() {
        return "Sent-состояние документа";
    }
}
