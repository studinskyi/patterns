package Creational.bihavioral.State;

public class SignedState implements State {
    String nameClient;

    public SignedState(String nameClient) {
        this.nameClient = nameClient;
    }

    @Override
    public void doAction(Context context) {
        System.out.println("Документ находится в состоянии Signed - " + nameClient);
        context.setState(this);
    }

    public String toString() {
        return "Signed-состояние документа - " + nameClient;
    }
}
