package Creational.bihavioral.State;

public class ConfirmationSentState implements State {
    @Override
    public void doAction(Context context) {
        System.out.println("Документ находится в состоянии ConfirmationSent");
        context.setState(this);
    }

    public String toString() {
        return "ConfirmationSent-состояние документа";
    }
}
