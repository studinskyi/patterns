package Creational.bihavioral.State;

public class LoadState implements State {
    @Override
    public void doAction(Context context) {
        System.out.println("Документ находится в состоянии Load");
        context.setState(this);
    }

    public String toString(){
        return "Load-состояние документа";
    }
}
