package Creational.bihavioral.State;

public class ReceivedClientState implements State {
    String nameClient;

    public ReceivedClientState(String nameClient) {
        this.nameClient = nameClient;
    }

    @Override
    public void doAction(Context context) {
        System.out.println("Документ находится в состоянии ReceivedClient - " + nameClient);
        context.setState(this);
    }

    public String toString() {
        return "ReceivedClient-состояние документа ( для клиента " + nameClient + ")";
    }
}
