package Creational.bihavioral.State;

public class StopState implements State{
    @Override
    public void doAction(Context context) {
        System.out.println("Документ находится в состоянии Stop");
        context.setState(this);
    }

    public String toString(){
        return "Stop-состояние документа";
    }
}
