package Creational.Prototype;

public class ComplicatedObject implements Copyable {
    TypeCopy type;

    public enum TypeCopy {
        ONE, TWO,
    }

    public ComplicatedObject copy() {
        return new ComplicatedObject();
    }

    public void setType(TypeCopy type) {
        this.type = type;
    }
}
