package Creational.Prototype;

public interface Copyable {
    Copyable copy();
}
